/**
 * Créé par Tony Blanchard le 12 novembre 2017.
 */
var referer = "";

/**
 * Un pointeur sur la dernière page activée avant l'évènement en cours de traitement.
 */
var dernièrePageActivée;

/**
 * Pages Une liste des pages visitées avec les préférences de chaque page stockées classées par les id des onglets à
 * partir desquels ces pages ont été chargées...
 *
 * @type {Array}
 */
var pages = [];
var ipRegex = /^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/;

/**
 * Définition de la liste blanche récupérée du localstorage
 *  listeBlanchePermanente: {
 *     domaine: {
 *        autorisations: [{hôte , hôteNormalisé}]
 *     },
 *     { ... }
 *   }
 * }
 */
var listeBlanchePermanente = {};
var journalActif = false;
var elasticStats = false;
var elasticPrefs = false;
var webRTCActif = false;

/*
 Définition de la page active

 pageActiveAuMomentDeLaRequête = {
 "nbRefus": nb de requêtes bloquées au total
 "hostname": "",
 "hôtesBloqués": ["",""],
 "urlInitiale": URL,
 "tabId": <num>
 "incognito: <true/false> depending on private browsing state
 "listeBlancheContextuelle" : [{hôte , hôteNormalisé}]
 }
 */

function putMappingConnexions() {
    if (elasticStats) {
        let mapping = {};
        mapping.settings = {
            number_of_shards: 1,
            number_of_replicas: 0,
            analysis: {
                analyzer: {
                    analyse_chemin: {
                        filter: [
                            "stop",
                            "lowercase",
                            "asciifolding"
                        ],
                        type: "custom",
                        tokenizer: "path_hierarchy"
                    }
                }
            }
        };
        mapping.mappings = {
            connexion: {
                properties: {
                    horodatage: {
                        type: "date"
                    },
                    host: {
                        type: "text",
                        fields: {
                            brut: {
                                type: "keyword",
                                ignore_above: 100
                            }
                        },
                        analyzer: "analyse_chemin",
                        search_analyzer: "standard"
                    },
                    hostInitiateur: {
                        type: "text",
                        fields: {
                            brut: {
                                type: "keyword",
                                ignore_above: 100
                            }
                        },
                        analyzer: "analyse_chemin",
                        search_analyzer: "standard"
                    },
                    hostRedirection: {
                        type: "text",
                        fields: {
                            brut: {
                                type: "keyword",
                                ignore_above: 100
                            }
                        },
                        analyzer: "analyse_chemin",
                        search_analyzer: "standard"
                    },
                    methode: {
                        type: "keyword",
                        ignore_above: 6
                    },
                    protocolRedirection: {
                        type: "text",
                        fields: {
                            keyword: {
                                type: "keyword",
                                ignore_above: 256
                            }
                        }
                    },
                    protocole: {
                        type: "keyword",
                        ignore_above: 20
                    },
                    protocoleRedirection: {
                        type: "keyword",
                        ignore_above: 20
                    },
                    raison: {
                        type: "text"
                    },
                    redirection: {
                        type: "boolean"
                    },
                    traitement: {
                        type: "keyword",
                        ignore_above: 10
                    },
                    type: {
                        type: "keyword",
                        ignore_above: 20
                    },
                    url: {
                        type: "text",
                        fields: {
                            brut: {
                                type: "keyword"
                            }
                        }
                    },
                    urlInitiatrice: {
                        type: "text",
                        fields: {
                            brut: {
                                type: "keyword"
                            }
                        }
                    },
                    urlPath: {
                        type: "text",
                        fields: {
                            keyword: {
                                type: "keyword",
                                ignore_above: 256
                            }
                        }
                    },
                    urlQuery: {
                        type: "text",
                        fields: {
                            keyword: {
                                type: "keyword",
                                ignore_above: 256
                            }
                        }
                    },
                    urlRedirection: {
                        type: "text",
                        fields: {
                            raw: {
                                type: "keyword"
                            }
                        }
                    },
                    urlRedirectionPath: {
                        type: "text",
                        fields: {
                            keyword: {
                                type: "keyword",
                                ignore_above: 256
                            }
                        }
                    },
                    urlRedirectionQuery: {
                        type: "text",
                        fields: {
                            keyword: {
                                type: "keyword",
                                ignore_above: 256
                            }
                        }
                    }
                }
            }
        };

        var http = new XMLHttpRequest();
        http.open("PUT", 'http://localhost:9200/vip_connexions', true);
        http.onerror = function () {
            if (http.status === 0) {
                if (journalActif) console.error(' ### Problème de connexion au moteur de recherche...');
            }
        }
        http.onloadend = function () {
            if (http.status < 200 || http.status >= 300) {
                if (http.status !== 409) {
                    if (journalActif) console.warn('### Erreur', http.status, http.statusText, '=> Impossible de positionner le mapping des connexions:', JSON.stringify(http.response));

                }
            } else {
                if (journalActif) console.info(' ###', JSON.stringify(http.response));
            }
        };
        http.send(JSON.stringify(mapping));
    }
}

function verifierMappingES() {
    if (elasticStats) {
        var http = new XMLHttpRequest();
        http.open("GET", 'http://localhost:9200/vip_connexions', true);
        http.onerror = function () {
            if (http.status === 0) {
                if (journalActif) console.error(' ### Problème de connexion au moteur de recherche...');
            }
        }
        http.onloadend = function () {
            if (http.status < 200 || http.status >= 300) {
                if (http.status !== 409) {
                    putMappingConnexions();
                    if (journalActif) console.warn('### Erreur', http.status, http.statusText, '=> Impossible de récupérer le mapping des connexions:', JSON.stringify(http.response));
                }
            } else {
                if (journalActif) console.info(' ###', JSON.stringify(http.response));
            }
        };
        http.send(null);
    }
}

function utiliserLeStockageElasticsearch() {
    elasticPrefs = true;
    sauverPréférences();
}

function utiliserLeStockageFirefox() {
    elasticPrefs = false;
    sauverPréférences();
}

function sauverPréférences() {
    if (elasticPrefs) {
        sauverPréférencesAvecElasticSearch();
    } else {
        browser.storage.local.set({listeBlanchePermanente: listeBlanchePermanente});
    }
}

function sauverPréférencesAvecElasticSearch() {
    let preference = listeBlanchePermanente;
    var http = new XMLHttpRequest();
    http.open("PUT", 'http://localhost:9200/vip/preference/1', true);
    http.onerror = function () {
        if (http.status === 0) {
            if (journalActif) console.error(' ### Problème de connexion au moteur de recherche...');
        }
    }
    http.onloadend = function () {
        if (http.status < 200 || http.status >= 300) {
            if (http.status !== 409) {
                if (journalActif) console.warn('### Erreur', http.status, http.statusText, '=> Impossible de sauvegarder la liste blanche:', JSON.stringify(http.response));
            }
        } else {
            if (journalActif) console.info(' ### préférence sauvegardéé:', preference);
            if (journalActif) console.info(' ###', JSON.stringify(http.response));
        }
    };
    http.send(JSON.stringify(preference));
}

function chargerPréférences() {
    if (elasticPrefs) {
        verifierMappingES();
        chargerPréférencesAvecElasticSearch();
    } else {
        browser.storage.local.get("listeBlanchePermanente").then(function (resultat) {
            // Initialize the saved stats if not yet initialized.
            if (!resultat.listeBlanchePermanente) {
                listeBlanchePermanente = {}
            } else {
                listeBlanchePermanente = resultat.listeBlanchePermanente;
                if (listeBlanchePermanente.journalActif !== undefined) {
                    journalActif = listeBlanchePermanente.journalActif;
                } else {
                    listeBlanchePermanente.journalActif = journalActif;
                }
                if (listeBlanchePermanente.elasticPrefs !== undefined) {
                    elasticPrefs = listeBlanchePermanente.elasticPrefs;
                } else {
                    listeBlanchePermanente.elasticPrefs = elasticPrefs;
                }
                if (listeBlanchePermanente.elasticStats !== undefined) {
                    elasticStats = listeBlanchePermanente.elasticStats;
                } else {
                    listeBlanchePermanente.elasticStats = elasticStats;
                }
                browser.privacy.network.peerConnectionEnabled.get({}).then(function(pref){
                    webRTCActif = pref.value;
                    console.info(' ### Etat du browser au lancement >>> journalActif:',journalActif,'elasticPrefs:', elasticPrefs, 'elasticStats:', elasticStats, 'webRTC actif:', webRTCActif);
                });
                console.info(" ### Préférences:",JSON.stringify(listeBlanchePermanente));
            }
        });
    }
}

function chargerPréférencesAvecElasticSearch() {

    var http = new XMLHttpRequest();
    http.open("GET", 'http://localhost:9200/vip/preference/1', true);
    http.onerror = function () {
        if (http.status === 0) {
            if (journalActif) console.error(' ### Problème de connexion au moteur de recherche...');
        }
    }
    http.onloadend = function () {
        if (http.status < 200 || http.status >= 300) {
            if (http.status !== 409) {
                if (journalActif) console.warn('### Erreur', http.status, http.statusText, '=> Impossible de récupérer la liste blanche:', JSON.stringify(http.response));
            }
        } else {
            let réponse = JSON.parse(http.response);
            if (journalActif) console.info('### Chargement des préférences:', réponse._source);
            listeBlanchePermanente = réponse._source;
            if (listeBlanchePermanente.journalActif) {
                journalActif = listeBlanchePermanente.journalActif;
            } else {
                listeBlanchePermanente.journalActif = journalActif;
            }
            if (listeBlanchePermanente.elasticPrefs) {
                elasticPrefs = listeBlanchePermanente.elasticPrefs;
            } else {
                listeBlanchePermanente.elasticPrefs = elasticPrefs;
            }
            if (listeBlanchePermanente.elasticStats) {
                elasticStats = listeBlanchePermanente.elasticStats;
            } else {
                listeBlanchePermanente.elasticStats = elasticStats;
            }
        }
    };
    http.send(null);
}

chargerPréférences();

/**
 * Gestion du texte du badge situé sur le bouton de gestion des domaines.
 *
 * @param pageActiveAuMomentDeLaRequête
 */
function majTexteBadge(pageActiveAuMomentDeLaRequête) {
    if (pageActiveAuMomentDeLaRequête.hôtesBloqués.length === 0 && pageActiveAuMomentDeLaRequête.listeBlancheContextuelle.length === 0){
        browser.browserAction.disable(pageActiveAuMomentDeLaRequête.tabId);
    } else {
        browser.browserAction.enable(pageActiveAuMomentDeLaRequête.tabId);
    }
    browser.browserAction.setBadgeText({
        text: pageActiveAuMomentDeLaRequête.hôtesBloqués.length.toString(),
        tabId: pageActiveAuMomentDeLaRequête.tabId
    });
}

// --- Gestion du filtrage ---
function tracerBlocage(requestURL, raisons, pageActiveAuMomentDeLaRequête) {

    if(elasticStats) {

        if ((pageActiveAuMomentDeLaRequête.incognito && pageActiveAuMomentDeLaRequête.incognito === false) || !pageActiveAuMomentDeLaRequête.incognito) {//Pour compatibilité avec les explorateurs non compatibles private browsing...

            let descriptionConnection = {};
            descriptionConnection.horodatage = new Date().getTime();
            descriptionConnection.methode = requestURL.method;
            descriptionConnection.protocole = requestURL.protocol;
            descriptionConnection.type = requestURL.type;
            descriptionConnection.url = requestURL.href;
            descriptionConnection.urlPath = requestURL.pathname;
            descriptionConnection.urlQuery = requestURL.search;
            descriptionConnection.host = requestURL.hôteNormalisé + '/' + requestURL.hostname;
            descriptionConnection.urlInitiatrice = requestURL.originUrl;
            if (pageActiveAuMomentDeLaRequête && pageActiveAuMomentDeLaRequête.urlInitiale) {
                descriptionConnection.hostInitiateur = pageActiveAuMomentDeLaRequête.urlInitiale.hôteNormalisé + '/' + pageActiveAuMomentDeLaRequête.urlInitiale.hostname;
            } else {
                descriptionConnection.hostInitiateur = "Firefox";
            }
            descriptionConnection.traitement = "bloquée";
            descriptionConnection.raison = raisons;

            var http = new XMLHttpRequest();
            http.open("POST", 'http://localhost:9200/vip_connexions/connexion', true);
            http.onerror = function () {
                if (http.status === 0) {
                    if (journalActif) console.error(' ### Problème de connexion au moteur de recherche...');
                }
            }
            http.onloadend = function () {
                if (http.status < 200 || http.status >= 300) {
                    if (http.status !== 409) {
                        if (journalActif) console.warn('### Erreur', http.status, http.statusText, '=> Impossible de tracer le blocage:', JSON.stringify(http.response));
                    }
                }
            };
            http.send(JSON.stringify(descriptionConnection));
        } else {
            raisons += ", non tracé, navigation privée..."
        }
    }
    if (journalActif) console.warn(' ### Blocage No', pageActiveAuMomentDeLaRequête.nbRefus.toString(), 'Page No', requestURL.tabId, ':', requestURL.method, requestURL.type, requestURL.href, 'origine:', requestURL.originUrl, raisons);
}

function tracerRequête(requestURL, raisons, pageActiveAuMomentDeLaRequête) {

    if (requestURL.hôteNormalisé !== 'localhost') {

        if(elasticStats) {
            let descriptionConnection = {};
            descriptionConnection.horodatage = new Date().getTime();
            descriptionConnection.methode = requestURL.method;
            descriptionConnection.protocole = requestURL.protocol;
            descriptionConnection.type = requestURL.type;
            descriptionConnection.url = requestURL.href;
            descriptionConnection.urlPath = requestURL.pathname;
            descriptionConnection.urlQuery = requestURL.search;
            descriptionConnection.host = requestURL.hôteNormalisé + '/' + requestURL.hostname;
            descriptionConnection.urlInitiatrice = requestURL.originUrl;
            if (pageActiveAuMomentDeLaRequête && pageActiveAuMomentDeLaRequête.urlInitiale) {
                descriptionConnection.hostInitiateur = pageActiveAuMomentDeLaRequête.urlInitiale.hôteNormalisé + '/' + pageActiveAuMomentDeLaRequête.urlInitiale.hostname;
            } else {
                descriptionConnection.hostInitiateur = "Firefox";
            }
            descriptionConnection.traitement = "exécutée";

            var http = new XMLHttpRequest();
            http.open("POST", 'http://localhost:9200/vip_connexions/connexion', true);
            http.onerror = function () {
                if (http.status === 0) {
                    if (journalActif) console.error(' ### Problème de connexion au moteur de recherche...');
                }
            }
            http.onloadend = function () {
                if (http.status < 200 || http.status >= 300) {
                    if (http.status !== 409) {
                        if (journalActif) console.warn('### Erreur', http.status, http.statusText, '=> Impossible de tracer la requête:', JSON.stringify(http.response));
                    }
                }
            };
            http.send(JSON.stringify(descriptionConnection));
        }
        if (journalActif) console.log('### Requête depuis la page No', requestURL.tabId, ':', requestURL.method, requestURL.type, requestURL.href, 'origine:', requestURL.originUrl);
    }
}

function tracerRedirection(requestURL, raisons, pageActiveAuMomentDeLaRequête, nouvelleAdresse) {

    if (requestURL.hôteNormalisé !== 'localhost') {
        let descriptionConnection = {};
        descriptionConnection.horodatage = new Date().getTime();
        descriptionConnection.methode = requestURL.method;
        descriptionConnection.protocole = requestURL.protocol;
        descriptionConnection.type = requestURL.type;
        descriptionConnection.url = requestURL.href;
        descriptionConnection.urlPath = requestURL.pathname;
        descriptionConnection.urlQuery = requestURL.search;
        descriptionConnection.host = requestURL.hôteNormalisé + '/' + requestURL.hostname;
        let redirection = new URL(nouvelleAdresse);
        descriptionConnection.protocolRedirection = redirection.protocol;
        descriptionConnection.urlRedirection = nouvelleAdresse;
        descriptionConnection.urlRedirectionPath = redirection.pathname;
        descriptionConnection.urlRedirectionQuery = redirection.search;
        descriptionConnection.hostRedirection = redirection.hostname;
        descriptionConnection.urlInitiatrice = requestURL.originUrl;
        if (pageActiveAuMomentDeLaRequête && pageActiveAuMomentDeLaRequête.urlInitiale) {
            descriptionConnection.hostInitiateur = pageActiveAuMomentDeLaRequête.urlInitiale.hôteNormalisé + '/' + pageActiveAuMomentDeLaRequête.urlInitiale.hostname;
        } else {
            descriptionConnection.hostInitiateur = "Firefox";
        }
        descriptionConnection.traitement = "redirigée";
        descriptionConnection.raison = raisons;

        var http = new XMLHttpRequest();
        http.open("POST", 'http://localhost:9200/vip_connexions/connexion', true);
        http.onerror = function () {
            if (http.status === 0) {
                if (journalActif) console.error(' ### Problème de connexion au moteur de recherche...');
            }
        }
        http.onloadend = function () {
            if (http.status < 200 || http.status >= 300) {
                if (http.status !== 409) {
                    if (journalActif) console.warn('### Erreur', http.status, http.statusText, '=> Impossible de tracer la requête:', JSON.stringify(http.response));
                }
            }
        };
        http.send(JSON.stringify(descriptionConnection));
        if (journalActif) console.warn(' ### Redirection depuis la page No', requestURL.tabId, ': url initiatrice=', requestURL.originUrl, ', url de pistage=', requestURL.href, ', nouvelle url=', nouvelleAdresse, raisons);
    }
}

/**
 * Si le nomActuel est 127.0.0.1, on remplace par localhost
 * Si le nomActuel contient un nom de domaine avec plus de deux composantes, on le réduit à ces seules composantes.
 *
 * Exemple: mail.google.com devient google.com
 *
 * Ainsi, des noms de sous-domaine ou de domaines frères auront le même nom normalisé.
 * Ceci permet d'éviter à l'utilisateur d'accepter tous les sous-domaines d'un domaine par des actions fastidieuses de
 * paramétrage et de simplifier les tests de filtrage.
 *
 * @param nomActuel Le nom à tester.
 * @returns {*} Le nom pris en compte pour le filtrage
 */
function normaliserNomDHôte(nomActuel) {
    if (nomActuel) {
        if (nomActuel === '127.0.0.1') {
            return 'localhost';
        } else {
            //On ne retient que les deux derniers common names des domaines pour permettre les domaines frères.Ex: mail.google.com et docs.google.com
            var names = nomActuel.split('.');
            if (names && names.length > 2) {
                nomActuel = names[names.length - 2] + '.' + names[names.length - 1];
            }
            return nomActuel;
        }
    } else {
        if (journalActif) console.warn(' ### Nom actuel ne devrait pas être vide...');
        return "";
    }
}

/**
 * Fonction d'interrogation des listes de domaines de la liste blanche ou refusés.
 *
 * @param liste La liste à traiter
 * @param hôteNormalisé Le nom d'hôte normalisé à chercher
 * @returns {boolean} retourne vrai lorsque la liste contient le nom d'hôte normalisé. Faux sinon.
 */
function contient(liste, hôteNormalisé) {
    for (let élément in liste) {
        if (liste[élément].hôteNormalisé === hôteNormalisé) {
            return true;
        }
    }
    return false;
}

/**
 * Fonction de retrait d'un nom d'hôte normalisé d'une liste de domaines si l'élément y est présent.
 *
 * @param liste La liste à traiter
 * @param hôteNormalisé Le nom d'hôte normalisé à chercher pour retrait
 */
function retirerDeLaListe(liste, hôte) {
    let indexElémentARetirer = -1;
    for (let élément in liste) {
        if (liste[élément].hôte === hôte) {
            indexElémentARetirer = élément;
        }
    }
    if (indexElémentARetirer > -1) {
        liste.splice(indexElémentARetirer, 1);
    }
}

/**
 * Bloque la requête en ajoutant l'hôte à la liste des hôtes bloqués si nécessaire et en mettant à jour le compteur
 * des blocages de la page sur le badge du bouton de gestion des domaines pour l'onglet actif
 *
 * @param requestURL La requête en cours de filtrage {requestURL}
 * @param raisons
 * @param pageActiveAuMomentDeLaRequête La page en cours de consultation {Page}
 * @returns {{cancel: boolean}}
 */
function bloquer(requestURL, raisons, pageActiveAuMomentDeLaRequête) {
    if (requestURL.tabId == pageActiveAuMomentDeLaRequête.tabId) {//On n'augmente pas les stats pour une autre page...
        if (requestURL.hôteNormalisé && !contient(pageActiveAuMomentDeLaRequête.hôtesBloqués, requestURL.hôteNormalisé)) {
            pageActiveAuMomentDeLaRequête.hôtesBloqués.push({
                hôte: requestURL.hôteNormalisé,
                hôteNormalisé: requestURL.hôteNormalisé
            });
            majTexteBadge(pageActiveAuMomentDeLaRequête);
        }
        pageActiveAuMomentDeLaRequête.nbRefus = pageActiveAuMomentDeLaRequête.nbRefus + 1;
    } else {
        raisons += ', requête effectuée depuis un autre onglet ou une autre fenêtre...';
        if (journalActif) {
            console.warn(" ### Requête effectuée depuis un autre onglet ou une autre fenêtre...");
        }
    }
    tracerBlocage(requestURL, raisons, pageActiveAuMomentDeLaRequête);
    return {"cancel": true};
}

function redirigerOuLaisserFaire(requestURL, raisons, pageActiveAuMomentDeLaRequête, nouvelleAdresse) {
    let origine = new URL(requestURL.originUrl);
    let destination = new URL(nouvelleAdresse);
    if (normaliserNomDHôte(destination.hostname) === normaliserNomDHôte(origine.hostname)) {
        return laisserFaire(requestURL, raisons, pageActiveAuMomentDeLaRequête);
    } else {
        tracerRedirection(requestURL, raisons, pageActiveAuMomentDeLaRequête, nouvelleAdresse);
        if (dernièrePageActivée.urlInitiale && !dernièrePageActivée.urlInitiale.hôteNormalisé) {
            dernièrePageActivée.urlInitiale.hôteNormalisé = normaliserNomDHôte(destination.hostname);
        }
        return {"redirectUrl": nouvelleAdresse};
    }
}

/**
 * Laisse passer la requête.
 *
 * @param requestURL La requête en cours de filtrage {requestURL}
 */
function laisserFaire(requestURL, raisons, pageActiveAuMomentDeLaRequête) {
    pageActiveAuMomentDeLaRequête.nbAcceptés = pageActiveAuMomentDeLaRequête.nbAcceptés + 1;
    majTexteBadge(pageActiveAuMomentDeLaRequête);
    tracerRequête(requestURL, raisons, pageActiveAuMomentDeLaRequête);
    return {"cancel": false};
}

/**
 * Indique si l'hôte de la requête cible doit être considéré comme un sous-domaine ou domaine frère en autorisant de fait
 * la requête émise...
 *
 * Ex: mail.google.com est frère du domaine calendar.google.com
 * Ex: mail.google.com est un domaine fils de google.com
 *
 * @param hôteCible Nom d'hôte normalisé
 * @returns {boolean}
 */
function accepterCeDomaineCommeFilsOuFrère(hôteCible) {
    for (var hôte in dernièrePageActivée.listeBlancheContextuelle) {
        if (dernièrePageActivée.listeBlancheContextuelle[hôte].hôteNormalisé === hôteCible) {
            return true;
        }
    }
    return false;
}

/**
 * Algorithme à utiliser pour décider du blocage si l'onglet initiateur de la requête est différent de l'onglet actuel.
 *
 * Règles appliquée:
 *   Si une requête est faite depuis un autre onglet que celui où nous sommes, et que cette requête n'est pas dirigée
 *   vers localhost, alors la requête doit être bloquée.
 *   Un exception à ce système si la requête depuis un autre onglet ou un autre contexte (tabId=-1 pour les favicons),on laisse tout de même passer...
 *
 *
 *
 * @param oùNousSommes La page actuellement affichée {Page}
 * @param oùNousAllons La requête en cours de filtrage {RequestURL}
 * @returns {boolean} vrai si la requête doit être bloquée
 */
function hôteBloquéSiRequêteDepuisAutrePage(oùNousSommes, oùNousAllons) {

    return oùNousSommes &&
        oùNousSommes.tabId &&
        oùNousSommes.tabId !== oùNousAllons.tabId && //La requête provient d'une autre page
        oùNousAllons.hôteNormalisé !== 'localhost' &&
        oùNousAllons.hôteNormalisé === oùNousSommes.hôteNormalisé;
}

/** Tous les appels vers moteurs de recherche doivent être acceptés.
 * Le mode de détection est pour le moins ... fragile ...
 */
function cibleEstRequêteMoteurDeRecherche(oùNousAllons) {

    if (oùNousAllons.href.includes("qwant.com/?q=") ||
        oùNousAllons.href.includes("qwantjunior.com/?q=") ||
        oùNousAllons.href.includes("/search?") ||
        oùNousAllons.href.startsWith("https://www.amazon.com/s/") ||
        oùNousAllons.href.includes("//www.amazon.com/exec/obidos/external-search/") ||
        oùNousAllons.href.startsWith("https://duckduckgo.com/?q=") ||
        oùNousAllons.href.includes("wikipedia.org/wiki/")){
        return true;
    }
}

/**
 * Algorithme à utiliser pour décider d'une autorisation de la reqête en cours
 *
 * Règle appliquée:
 *
 *   Si la page actuelle n'a pas encore d'hôte attitré et autorisé ou
 *   si l'hôte cible est égal au domaine de la page actuelle ou
 *   si l'hôte cible est un sous-domaine ou domaine frère des domaines des pages de la liste blanche
 *   si la page actuelle ou la page de destination est sur localhost
 *   alors, la requête doit être autorisée
 *
 * @param pageActuelle La page actuellement affichée {Page}
 * @param oùNousAllons La requête en cours de filtrage {RequestURL}
 * @returns {boolean} indique si l'hôte doit être autorisé ou non
 */
function hôteAutorisé(pageActuelle, oùNousAllons) {

    return !pageActuelle.urlInitiale || !pageActuelle.urlInitiale.hôteNormalisé || //Si la page actuelle n'a pas encore d'hôte attitré et autorisé...
        pageActuelle.urlInitiale.hôteNormalisé === oùNousAllons.hôteNormalisé ||
        accepterCeDomaineCommeFilsOuFrère(oùNousAllons.hôteNormalisé) ||
        pageActuelle.urlInitiale.hôteNormalisé === 'localhost' || //Toujours autoriser de sortir si nous sommes sur localhost...
        oùNousAllons.hôteNormalisé === 'localhost' || //Toujours autoriser d'aller vers localhost !
        cibleEstRequêteMoteurDeRecherche(oùNousAllons);
}

function decider(requestURL) {
    if (hôteBloquéSiRequêteDepuisAutrePage(dernièrePageActivée, requestURL)) {
        return bloquer(requestURL, 'abandonnée car ne provenant pas de cette page et absent de la liste blanche...', dernièrePageActivée);
    }
    if (pages[requestURL.tabId]) {//Attention, ne pas simplifier ici.
        if (hôteAutorisé(pages[requestURL.tabId], requestURL)) {
            return laisserFaire(requestURL, '', dernièrePageActivée);
        } else {
            return bloquer(requestURL, 'abandonnée', dernièrePageActivée);
        }
    }
    return laisserFaire(requestURL, '', dernièrePageActivée);
}

/**
 * Si l'utilsateur est devant son ordinateur, la requête est passée dans deux filtres:
 *   1- Filtre d'une requête issue d'une autre page que la page actuelle
 *   2- Filtre standard d'autorisation
 *
 * @param requestDetails
 * @returns {Objet} { cancel: trueou false } selon décision de blocage.
 */
function filtrerDomaine(requestDetails) {

    var requestURL = new URL(requestDetails.url);
    requestURL.tabId = requestDetails.tabId;
    requestURL.hôteNormalisé = normaliserNomDHôte(requestURL.hostname);
    requestURL.method = requestDetails.method;
    requestURL.type = requestDetails.type;
    requestURL.originUrl = requestDetails.originUrl;

    //Filtrage des urls de renvoi (une url contenenant un redirect dès la première page visitée: Ex unmoteur de recherche
    // renvoie vers son serveur avant de rediriger sur le lien cible)
    let positionAutreAdresse = requestDetails.url.lastIndexOf("http");

    if (positionAutreAdresse > 0) { //Le site contient une url de renvoie
        let pattern = /\=(http[s]?[\%0-9A-Z]{6}[a-zA-Z0-9-_\.\/\%]*)&/g
        let nomSiteCible = pattern.exec(requestDetails.url);

        if (nomSiteCible[1]) { //L'url est encodée
            //Si la page !pages[requestDetails.tabId] n'existe pas et que l'on a une url encapsulée dans une url,il s'agit de pistage.
            //Dans les autres cas, il pourrait s'agir d'une connection normale comme OpenID par exemple
            return redirigerOuLaisserFaire(requestURL, 'Evite le pistage...', dernièrePageActivée, decodeURIComponent(nomSiteCible[1]));
        }
    }

    if (dernièrePageActivée.urlInitiale && !dernièrePageActivée.urlInitiale.hôteNormalisé) {
        dernièrePageActivée.urlInitiale.hôteNormalisé = normaliserNomDHôte(dernièrePageActivée.urlInitiale.hostname);
    }

    return decider(requestURL);
}

/**
 * Ajout d'un listener de filtrage des requêtes http
 */
browser.webRequest.onBeforeRequest.addListener(
    filtrerDomaine,
    {urls: ["<all_urls>"]},
    ["blocking"]
);

// --- Gestion de l'anonymisation ---

/**
 * Fonction de réécriture des en-têtes User-Agent, Referer et Host
 * @param requête
 * @returns {{requestHeaders: *}}
 */
function réécrireLesEnTêtes(requête) {
    let urlRequête = new URL(requête.url);
    for (var enTête in requête.requestHeaders) {

        switch (requête.requestHeaders[enTête].name.toLowerCase()) {
            /*case  "user-agent":
                requête.requestHeaders[enTête].value = user_agent;
                break;*/
            case 'host':
                if (ipRegex.test(urlRequête.hostname)) {
                    if (urlRequête.hostname === '127.0.0.1') {
                        requête.requestHeaders[enTête].value = 'localhost';
                    }
                }
                break;
            case 'referer':
                requête.requestHeaders[enTête].value = referer;
                break;
            default:
                break;
        }
    }
    return {requestHeaders: requête.requestHeaders};
}

/**
 * Ajout d'un listener de réécriture des headers User-Agent, Referer et Host
 */
browser.webRequest.onBeforeSendHeaders.addListener(
    réécrireLesEnTêtes,
    {urls: ["<all_urls>"]},
    ["blocking", "requestHeaders"]
);

// --- Gestion de la navigation de l'utilisateur et de récupération des données des pages visitées ---

/** infoPageActive contient un champ tabId et windowId */
function activationPage(infoPageActive) {
    if (pages[infoPageActive.tabId]) {
        if (journalActif) console.log(" ### Activation page No ", infoPageActive.tabId);
        dernièrePageActivée = pages[infoPageActive.tabId];
        if (dernièrePageActivée.urlInitiale) {
            if (journalActif)
                console.info(" ### URL initiale de la page: ", dernièrePageActivée.urlInitiale.href, "Hôte autorisé pour cette page: ", dernièrePageActivée.urlInitiale.hostname);
        }
    } else {
        console.warn(" ### Page inconue:", infoPageActive);
        créerNouvellePage(infoPageActive.tabId);
        dernièrePageActivée = pages[infoPageActive.tabId];
        dernièrePageActivée.incognito = infoPageActive.incognito;
    }

    majTexteBadge(dernièrePageActivée);
}

function créerNouvellePage(tabId) {
    pages[tabId] = {urlInitiale: {}, nbRefus: 0, nbAcceptés: 0}
}

function créationPage(pageActive) {
    if (journalActif) console.info(" ### Création page No ", pageActive.id);
    créerNouvellePage(pageActive.id);
    dernièrePageActivée = pages[pageActive.id];
    dernièrePageActivée.tabId = pageActive.id;
    dernièrePageActivée.listeBlancheContextuelle = [];
    dernièrePageActivée.hôtesBloqués = [];
}

function majPage(idPage, infoChgt, pageActive) {

    if (infoChgt && infoChgt.status) {
        if (!pages[idPage]) {
            créationPage(pageActive);
        }
        if (pageActive.url && pageActive.url.length > 0) {
            dernièrePageActivée = pages[idPage];
            dernièrePageActivée.incognito = pageActive.incognito;
            if (pageActive.url != "about:blank" && pageActive.url != "about:newtab") {
                dernièrePageActivée.urlInitiale = new URL(pageActive.url);
                dernièrePageActivée.urlInitiale.hôteNormalisé = normaliserNomDHôte(dernièrePageActivée.urlInitiale.hostname);
                if (dernièrePageActivée.urlInitiale.hôteNormalisé && dernièrePageActivée.urlInitiale.hôteNormalisé.length > 0) {
                    if (listeBlanchePermanente[dernièrePageActivée.urlInitiale.hôteNormalisé]) {
                        if (dernièrePageActivée.incognito) {
                            if (dernièrePageActivée.listeBlancheContextuelle.length === 0) {//On ne duplique que si besoin...
                                dernièrePageActivée.listeBlancheContextuelle = listeBlanchePermanente[dernièrePageActivée.urlInitiale.hôteNormalisé].autorisations.slice(0); //clône du tableau
                            }
                        } else {
                            dernièrePageActivée.listeBlancheContextuelle = listeBlanchePermanente[dernièrePageActivée.urlInitiale.hôteNormalisé].autorisations;
                        }
                    } else {
                        if (dernièrePageActivée.incognito) {
                            listeBlanchePermanente[dernièrePageActivée.urlInitiale.hôteNormalisé] = {autorisations: []};
                        }
                        dernièrePageActivée.listeBlancheContextuelle = [];
                    }
                    if (journalActif) console.log(" ### Chargement page No", idPage, infoChgt.status, ", url:", dernièrePageActivée.urlInitiale.href, " hôte autorisé:", dernièrePageActivée.urlInitiale.hostname);
                } else {
                    if (journalActif) console.log(" ### Chargement page vide No", idPage, infoChgt.status, ", url:", dernièrePageActivée.urlInitiale.href, " hôte autorisé:", dernièrePageActivée.urlInitiale.hostname);
                }
            } else {
                if (journalActif) console.log(" ### Url 'about:blank'non prise en compte pour la restriction initiale de domaine...");
            }
        } else {
            dernièrePageActivée = pages[idPage];
            dernièrePageActivée.incognito = pageActive.incognito;
            if (journalActif) console.log(" ### Url 'about:blank'non prise en compte pour la restriction initiale de domaine...");
        }
    }
}

/**
 * Ajout des méthodes de rappel pour chaque activité sur les onglets de Firefox
 * Permet d'écouter les actions de l'utilisateur pour charger l'état d'autorisation en fonction de l'onglet courant
 */
browser.tabs.onActivated.addListener(activationPage);
browser.tabs.onCreated.addListener(créationPage);
browser.tabs.onUpdated.addListener(majPage);

// --- Gestion de la liste blanche permanente et des préférences

function majPreferences(pageActive) {
    if((pageActive.incognito && pageActive.incognito === false) || !pageActive.incognito) {//Pour compativbilité avec les explorateurs non compatible private browsing...

        if (pageActive.listeBlancheContextuelle.length > 0) {
            listeBlanchePermanente[dernièrePageActivée.urlInitiale.hôteNormalisé] = {autorisations: pageActive.listeBlancheContextuelle};
        } else {
            delete listeBlanchePermanente[dernièrePageActivée.urlInitiale.hôteNormalisé];
        }
        sauverPréférences();

    } else {
        if (journalActif) console.warn(' ### Préférences non sauvegardées durant une navigation privée...');
    }
}

// --- Gestion de la communication entre les différents modules  ---

/**
 * Fonction de communication inter modules du plugin
 * @param message La clef de message qui permet de savoir l'ation à exécuter
 * @param expéditeur
 * @param méthodeDeRappelRéponse Méthode à rappeler en cas de communication double sens
 */
function gestionMessagesEntrantSortants(message, expéditeur, méthodeDeRappelRéponse) {
    if (message.msg === 'listeBlanche') {
        var listeBlancheAPrendreEnCompte = dernièrePageActivée.listeBlancheContextuelle;
        méthodeDeRappelRéponse({
            response: listeBlancheAPrendreEnCompte,
            hôteCourant: dernièrePageActivée.urlInitiale.hôteNormalisé,
            refus: dernièrePageActivée.nbRefus,
            acceptés: dernièrePageActivée.nbAcceptés,
            journalisation: journalActif,
            elasticPrefs: elasticPrefs,
            elasticStats: elasticStats,
            webRTC: webRTCActif});
    } else if (message.msg === 'journalisation') {
        listeBlanchePermanente.journalActif = message.journalisation;
        journalActif = message.journalisation;
        majPreferences(dernièrePageActivée)
    } else if (message.msg === 'elasticPrefs') {
        if (message.elasticPrefs === true) {
            utiliserLeStockageElasticsearch();
        } else {
            utiliserLeStockageFirefox();
        }
        listeBlanchePermanente.elasticPrefs = message.elasticPrefs;
        majPreferences(dernièrePageActivée);
    } else if (message.msg === 'elasticStats') {
        elasticStats = message.elasticStats;
        listeBlanchePermanente.elasticStats = message.elasticStats;
        majPreferences(dernièrePageActivée);
    } else if (message.msg === 'webRTC') {
        webRTCActif =  message.webRTC;
        browser.privacy.network.peerConnectionEnabled.set({value: message.webRTC});
    } else if (message.msg === 'listeNoire') {
        méthodeDeRappelRéponse({response: dernièrePageActivée.hôtesBloqués});
    } else if (message.msg === 'hôteAutoriséPourLaPage') {

        let hôteNormalisé = normaliserNomDHôte(message.hôte);
        dernièrePageActivée.listeBlancheContextuelle.push({hôte: hôteNormalisé, hôteNormalisé: hôteNormalisé});
        retirerDeLaListe(dernièrePageActivée.hôtesBloqués, hôteNormalisé);
        majTexteBadge(dernièrePageActivée);
        majPreferences(dernièrePageActivée)

    } else if (message.msg === 'hôteBanniPourLaPage') {

        let hôteNormalisé = normaliserNomDHôte(message.hôte);
        dernièrePageActivée.hôtesBloqués.push({hôte: hôteNormalisé, hôteNormalisé: hôteNormalisé});
        retirerDeLaListe(dernièrePageActivée.listeBlancheContextuelle, hôteNormalisé);
        majTexteBadge(dernièrePageActivée);
        majPreferences(dernièrePageActivée)
    }
}

/**
 *  Ajout du listener de communication entre modules
 */
browser.runtime.onMessage.addListener(gestionMessagesEntrantSortants);

browser.browserAction.disable();

/**
 * Tests:
 *
 * 1. Tester le private browsing:
 *   - Ouvrir un site comme www.les-crises.fr et noter le nombre de blocages (N)
 *   - Passer en navigation privée
 *   - Ouvrir dans un onglet O1 en navigation privée le site www.les-crises.fr et vérifier nombre de blocages = N
 *   - Dans l'onglet en navigation privée, ajouter un domaine à la liste blanche,  recharger la page et vérifier nombre de blocages = N-1
 *   - Ouvrir un nouvel onglet O2 en navigation privée le site www.les-crises.fr et vérifier nombre de blocages = N
 *   - Retourner sur O1 et ajouter un domaine à la liste blanche, recharger la page et vérifier nombre de blocages = N-2
 *   - Retourner sur O2, recharger la page et vérifier nombre de blocages = N
 *   - Retourner sur l'onglet en navigation classique,recharger la page et vérifier nombre de blocages = N
 *
 * 2. Tester la sortie vers les moteurs de recherche
 *   - Ouvrir un site comme www.les-crises.fr  et noter le nombre de blocages (N)
 *   - Tester de changement d'adresse dans la barre d'adresse avec la valeur: www.google.com et tester blocage = N+1
 *   - Tester de saisir de texte dans le champs des moteurs de recherche, valider et vérifier accès à la page de résultat du moteur par défaut
 *   - Tester le retour arrière et vérifier blocage
 *   - Tester de changer de moteur de recherche à partir du menu "loupe" du champs des moteurs de recherche et vérifier accès au à la page de résultat du bon moteur de recherche
 *   - Réitérer pour tous moteurs de recherche
 */
