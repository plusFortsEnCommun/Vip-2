var l = document.links;
var nbLiens = l.length;
function estPageDeMoteurDeRecherche(domaine){
    return domaine.endsWith('qwant.com') || //Pour ne pas être bloqués lorsque les liens des résultats passent par le moteur de recherche pour redirection ultérieure
        domaine.endsWith('qwantjunior.com') ||
        domaine === 'duckduckgo.com' ||
        domaine.endsWith('google.fr') ||
        domaine.endsWith('search.yahoo.com') ||
        domaine.endsWith('bing.com');
}
for(var i=0; i<nbLiens; i++) {
    let patt = /^http[s]?:\/\/([0-9]*[w]{0,3}[\.]{0,1}[a-zA-Z0-9\.]*)\/.*/g;
    let result = patt.exec(l[i].href);

    if (result && result.length > 1 ){
         if (document.location.host !== result[1] || //Les liens sortants du site s'ouvrent dans d'autres onglets...
            estPageDeMoteurDeRecherche(result[1]))
         {l[i].target = '_blank';}
    }
}
if (document.location.host.startsWith('localhost') === false) {
    if (localStorage)  {
        //Sauvegarde un éventuel jeton jwt
        var jwt = localStorage.getItem('jwt');
        localStorage.clear();
        localStorage.setItem('jwt', jwt);
    }
    if (window && window.localStorage) {window.localStorage.clear();}
}
